# clear-dispatch

dispatch app

Database Constraints
```bash
node migrations/constraints.js
```

Dev SSL
```bash
cd cert/dev
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
openssl rsa -passin pass:x -in server.pass.key -out server.key
rm server.pass.key
openssl req -new -key server.key -out server.csr
openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt


cd cert/dev
openssl genrsa -des3 -passout pass:x -out bundle.pass.key 2048
openssl rsa -passin pass:x -in bundle.pass.key -out bundle.key
rm bundle.pass.key
openssl req -new -key bundle.key -out bundle.csr
openssl x509 -req -sha256 -days 365 -in bundle.csr -signkey bundle.key -out bundle.crt
```

Dev DATA
```bash
node migrations/demo.js
```
