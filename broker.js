const SCBroker = require('socketcluster/scbroker')

class Broker extends SCBroker {
  run () {
    console.log('   >> Broker PID:', process.pid)
    require('sc-redis').attach(this)
  }
}

/* eslint-disable no-new */
new Broker()
