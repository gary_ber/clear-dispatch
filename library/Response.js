module.exports = class Response {
  /**
   *
   * @param res
   * @param body
   */
  constructor (res, body) {
    this.res = res
    this.result = {
      success: true
    }
    if (body) this.result.body = body
  }

  /**
   *
   * @param error
   * @returns {Response}
   */
  addError (error) {
    if (error) {
      this.result.success = false
      if (!this.result.errors) this.result.errors = []
      this.result.errors.push(error)
    }
    return this
  }

  /**
   *
   * @param warning
   * @returns {Response}
   */
  addWarning (warning) {
    if (warning) {
      if (!this.result.warnings) this.result.warnings = []
      this.result.warnings.push(warning)
    }
    return this
  }

  /**
   *
   */
  send () {
    this.res.setHeader('Content-Type', 'application/json')
    if (!this.result.error) this.result.success = true
    return this.res.send(JSON.stringify(this.result))
  }
}
