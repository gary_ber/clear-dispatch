module.exports = class BaseController {
  constructor (client, neo4jDriver) {
    this.client = client
    this.neo4jDriver = neo4jDriver
    this.requireAuth = true
  }
}
