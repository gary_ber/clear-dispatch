module.exports = class SocketResponse {
  /**
   *
   */
  constructor (scRespond) {
    this.result = {
      success: true,
      controller_name: null,
      controller_method: null,
      body: null
    }
    this.scRespond = scRespond
  }

  setBody (body) {
    if (body) this.result.body = body
    return this.generate()
  }

  setControllerName (controllerName) {
    if (controllerName) {
      this.result.controller_name = controllerName
    }
    return this.generate()
  }

  setControllerMethod (controllerMethod) {
    if (controllerMethod) {
      this.result.controller_method = controllerMethod
    }
    return this.generate()
  }

  addError (error) {
    if (error) {
      this.result.success = false
      if (!this.result.errors) this.result.errors = []
      this.result.errors.push(error)
    }
    return this.generate()
  }

  addWarning (warning) {
    if (warning) {
      if (!this.result.warnings) this.result.warnings = []
      this.result.warnings.push(warning)
    }
    return this.generate()
  }

  generate () {
    return this.result
  }

  respond (body) {
    this.setBody(body)
    return this.scRespond(null, this.generate())
  }
}
