const crypto = require('crypto')

module.exports = class Security {
  static passwordHash (password) {
    return crypto.createHash('sha256').update(password).digest('base64')
  }
}
