const neo4j = require('neo4j-driver').v1
const path = require('path')
const env = process.env.NODE_ENV || 'development'

process.env = require(path.join(__dirname, '../config/config.json'))[env]

if (!process.env.NEO4J_ADDRESS) process.env.NEO4J_ADDRESS = 'localhost'
if (!process.env.NEO4J_PORT) process.env.NEO4J_PORT = 7687
if (!process.env.NEO4J_USER) process.env.NEO4J_USER = 'neo4j'
if (!process.env.NEO4J_PASS) process.env.NEO4J_PASS = 'test'

const neo4jDriver = neo4j.driver('bolt://' + process.env.NEO4J_ADDRESS + ':' + process.env.NEO4J_PORT, neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASS))

// query

const session = neo4jDriver.session()

let queries = [
  // 'CREATE CONSTRAINT ON (a:User) ASSERT exists(a.email)',
  'CREATE CONSTRAINT ON (a:User) ASSERT a.email IS UNIQUE',
  // 'CREATE CONSTRAINT ON (a:User) ASSERT exists(a.name)',
  // 'CREATE CONSTRAINT ON (a:User) ASSERT exists(a.id)',
  'CREATE CONSTRAINT ON (a:User) ASSERT a.id IS UNIQUE'
  // 'CREATE CONSTRAINT ON (a:User) ASSERT exists(a.timestamp)',
]

queries.forEach(function (query) {
  const resultPromise = session.run(query)
  resultPromise.then(result => {
    session.close()
    // neo4jDriver.close()
  })
})
