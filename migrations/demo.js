const neo4j = require('neo4j-driver').v1
const path = require('path')
const env = process.env.NODE_ENV || 'development'
const Security = require('../library/Security')

process.env = require(path.join(__dirname, '../config/config.json'))[env]

if (!process.env.NEO4J_ADDRESS) process.env.NEO4J_ADDRESS = 'localhost'
if (!process.env.NEO4J_PORT) process.env.NEO4J_PORT = 7687
if (!process.env.NEO4J_USER) process.env.NEO4J_USER = 'neo4j'
if (!process.env.NEO4J_PASS) process.env.NEO4J_PASS = 'test'

const neo4jDriver = neo4j.driver('bolt://' + process.env.NEO4J_ADDRESS + ':' + process.env.NEO4J_PORT, neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASS))

// query

const session = neo4jDriver.session()

const resultPromise = session.run(
  'MERGE (id:UniqueId{name:\'User\'}) ' +
  'ON CREATE SET id.count = 1 ' +
  'ON MATCH SET id.count = id.count + 1 ' +
  'WITH id.count AS uid ' +
  'CREATE (a:User { id: uid, uuid: apoc.create.uuid(), name: {name}, email: {email}, password: {password}, timestamp: timestamp() }) ' +
  'RETURN a',
  {
    name: 'Gary',
    email: 'gary.bernatonis@gmail.com',
    password: Security.passwordHash('test')
  }
)

resultPromise.then(result => {
  session.close()

  console.log('Ran demo migration.')

  // on application exit:
  neo4jDriver.close()
})
