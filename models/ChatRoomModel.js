const User = require('./neo4j/User')
const ChatRoom = require('./neo4j/ChatRoom')

module.exports = class ChatRoomModel {
  constructor (neo4jDriver) {
    this.neo4jDriver = neo4jDriver
  }

  toggleOpen (chatRoom, user, open) {
    const session = this.neo4jDriver.session()
    return session.run(
      [
        'MATCH (user:User) WHERE user.uuid = {user_uuid}',
        'WITH user',
        'MATCH (chatRoom:ChatRoom) WHERE chatRoom.uuid = {chat_room_uuid}',
        'WITH user, chatRoom',
        'MATCH (user)-[attends:ATTENDS]-(chatRoom)',
        'SET attends.open = {open}',
        'RETURN user, chatRoom, attends'
      ].join('\n'),
      {
        chat_room_uuid: chatRoom.uuid,
        user_uuid: user.uuid,
        open: open
      }
    )
      .then(result => {
        session.close()
        return this.find(chatRoom.uuid)
      })
  }

  toggleListening (chatRoom, user, listening) {
    const session = this.neo4jDriver.session()
    return session.run(
      [
        'MATCH (user:User) WHERE user.uuid = {user_uuid}',
        'WITH user',
        'MATCH (chatRoom:ChatRoom) WHERE chatRoom.uuid = {chat_room_uuid}',
        'WITH user, chatRoom',
        'MATCH (user)-[attends:ATTENDS]-(chatRoom)',
        'SET attends.listening = {listening}',
        'RETURN user, chatRoom, attends'
      ].join('\n'),
      {
        chat_room_uuid: chatRoom.uuid,
        user_uuid: user.uuid,
        listening: listening
      }
    )
      .then(result => {
        session.close()
        return this.find(chatRoom.uuid)
      })
  }

  create (users) {
    const session = this.neo4jDriver.session()
    // TODO only create if all Users aren't already in a ChatRoom together
    return session.run(
      [
        'CREATE (chatRoom:ChatRoom {uuid: apoc.create.uuid(), timestamp: timestamp()})',
        'WITH chatRoom, {users} AS users',
        'UNWIND users AS user',
        'MATCH (newUser:User) WHERE newUser.uuid = user.uuid',
        'MERGE (newUser)-[attends:ATTENDS { open: true, listening: true } ]->(chatRoom)',
        'RETURN chatRoom, collect(attends), collect(newUser)'
      ].join('\n'), {
        users: users
      })
      .then(result => {
        session.close()
        if (!result.records.length) {
          return false
        }
        let chatRoom = new ChatRoom(node)
        return this.find(chatRoom.uuid)
      })
  }

  find (chatRoomUUID) {
    const session = this.neo4jDriver.session()
    return session.run(
      [
        'MATCH (chatRoom:ChatRoom) WHERE chatRoom.uuid = {chat_room_uuid}',
        'WITH chatRoom',
        'MATCH (user:User)-[attends:ATTENDS]-(chatRoom)',
        'RETURN chatRoom, collect(user), collect(attends)'
      ].join('\n'),
      {
        chat_room_uuid: chatRoomUUID
      }
    )
      .then(result => {
        session.close()
        if (!result.records.length) {
          return false
        }
        const node = result.records[0].get('chatRoom')
        let chatRoom = new ChatRoom(node)
        if (chatRoom) {
          let attendants = result.records[0].get('collect(attends)')
          chatRoom.attendants = []
          result.records[0].get('collect(user)').forEach((userNode, index) => {
            chatRoom.attendants[index] = attendants[index].properties
            chatRoom.attendants[index].user = new User(userNode)
          })
        }
        return chatRoom
      })
  }

  all (userUUID) {
    const session = this.neo4jDriver.session()
    return session.run(
      'MATCH (a:User { uuid: {user_uuid} }) WITH a ' +
      'MATCH (b:ChatRoom)<-[:ATTENDS]-(a) WITH a, b ' +
      'MATCH (c:User)-[d:ATTENDS]->(b) ' +
      'RETURN b, collect(d), collect(c)',
      {
        user_uuid: userUUID
      }
    )
      .then(results => {
        session.close()
        let chatRooms = []
        results.records.forEach(res => {
          let chatRoom = new ChatRoom(res.get('b'))
          let attendants = res.get('collect(d)')
          chatRoom.attendants = []
          res.get('collect(c)').forEach((userNode, index) => {
            chatRoom.attendants[index] = attendants[index].properties
            chatRoom.attendants[index].user = new User(userNode)
          })
          chatRooms.push(chatRoom)
        })
        return chatRooms
      })
  }
}
