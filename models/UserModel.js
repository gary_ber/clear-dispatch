const User = require('./neo4j/User')
const Security = require('../library/Security')

module.exports = class UserModel {
  constructor (neo4jDriver) {
    this.neo4jDriver = neo4jDriver
  }

  create (name, email, password) {
    const session = this.neo4jDriver.session()
    return session.run(
      'MERGE (id:UniqueId{name:\'User\'}) ' +
      'ON CREATE SET id.count = 1 ' +
      'ON MATCH SET id.count = id.count + 1 ' +
      'WITH id.count AS uid ' +
      'CREATE (a:User { id: uid, uuid: apoc.create.uuid(), name: {name}, email: {email}, password: {password}, timestamp: timestamp() }) ' +
      'RETURN a LIMIT 1',
      {
        name: name,
        email: email,
        password: Security.passwordHash(password)
      }
    )
      .then(result => {
        session.close()
        const node = result.records[0].get('a')
        return new User(node)
      })
  }

  find (id) {
    const session = this.neo4jDriver.session()
    return session.run(
      'MATCH (a:User {id: {id} }) ' +
      'RETURN a',
      {
        id: id
      }
    )
      .then(result => {
        session.close()
        if (!result.records.length) {
          return false
        }
        const node = result.records[0].get('a')
        return new User(node)
      })
  }

  update (user) {
    const session = this.neo4jDriver.session()
    const userUUID = user.uuid
    delete user.id
    delete user.uuid
    delete user.password
    return session.run(
      [
        'WITH {user} AS user',
        'UNWIND user AS u',
        'MATCH (a:User) WHERE a.uuid = {user_uuid}',
        'WITH a, u',
        'SET a += u',
        'RETURN a'
      ].join('\n'),
      {
        user_uuid: userUUID,
        user: user
      }
    )
      .then(result => {
        session.close()
        if (!result.records.length) {
          return false
        }
        const node = result.records[0].get('a')
        return new User(node)
      })
  }

  destroy (id) {
    const session = this.neo4jDriver.session()
    return session.run(
      'MATCH (a:User {id: { id }}) ' +
      'DETACH DELETE a',
      {
        id: id
      }
    )
      .then(result => {
        session.close()
        return true
      })
  }

  all () {
    const session = this.neo4jDriver.session()
    return session.run(
      'MATCH (a:User) ' +
      'RETURN a'
    )
      .then(results => {
        session.close()
        let users = []
        results.records.forEach(res => {
          let node = res.get(0)
          users.push(new User(node))
        })
        return users
      })
  }
}
