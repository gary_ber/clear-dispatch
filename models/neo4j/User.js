module.exports = class User {
  constructor (_node) {
    let user = _node.properties
    user.password = undefined
    Object.keys(user).forEach((prop) => {
      this[prop] = user[prop]
    })
    if (user.id) {
      this.id = user.id.toNumber()
    }
    if (user.timestamp) {
      this.timestamp = user.timestamp.toNumber()
    }
  }
}
