module.exports = class ChatRoom {
  constructor (_node) {
    let chatRoom = _node.properties
    Object.keys(chatRoom).forEach((prop) => {
      this[prop] = chatRoom[prop]
    })
    if (chatRoom.timestamp) {
      this.timestamp = chatRoom.timestamp.toNumber()
    }
  }
}
