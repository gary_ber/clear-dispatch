module.exports = class ChatMessage {
  constructor (_node) {
    let chatMessage = _node.properties
    Object.keys(chatMessage).forEach((prop) => {
      this[prop] = chatMessage[prop]
    })
    if (chatMessage.timestamp) {
      this.timestamp = chatMessage.timestamp.toNumber()
    }
  }
}
