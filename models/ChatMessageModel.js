const ChatMessage = require('./neo4j/ChatMessage')
const ChatRoom = require('./neo4j/ChatRoom')
const User = require('./neo4j/User')

module.exports = class ChatMessageModel {
  constructor (neo4jDriver) {
    this.neo4jDriver = neo4jDriver
  }

  create (userUUID, chatRoomUUID, content) {
    const session = this.neo4jDriver.session()
    return session.run(
      'MATCH (user:User { uuid: {user_uuid} }) ' +
      'WITH user ' +
      'MATCH (chatRoom:ChatRoom { uuid: {chat_room_uuid} }) ' +
      'CREATE (chatMessage:ChatMessage { content: {content}, uuid: apoc.create.uuid(), timestamp: timestamp() }) ' +
      'CREATE (user)-[:SENT]->(chatMessage)-[:TO]->(chatRoom) ' +
      'RETURN user, chatRoom, chatMessage',
      {
        user_uuid: userUUID,
        chat_room_uuid: chatRoomUUID,
        content: content
      }
    )
      .then(result => {
        session.close()
        if (!result.records.length) {
          return false
        }
        const singleRecord = result.records[0]
        let chatMessage = new ChatMessage(singleRecord.get('chatMessage'))
        let chatRoom = new ChatRoom(singleRecord.get('chatRoom'))
        let user = new User(singleRecord.get('user'))
        if (chatMessage) {
          chatMessage.chatRoom = chatRoom
          chatMessage.user = user
        }
        return chatMessage
      })
  }

  all (chatRoomUUID) {
    const session = this.neo4jDriver.session()
    return session.run(
      'MATCH (a:ChatRoom { uuid: {chat_room_uuid} }) WITH a ' +
      'MATCH (b:ChatMessage)-[:TO]->(a) ' +
      'RETURN b ' +
      'ORDER BY b.timestamp ASC',
      {
        chat_room_uuid: chatRoomUUID
      }
    )
      .then(results => {
        session.close()
        let chatMessages = []
        results.records.forEach(res => {
          let chatMessage = new ChatMessage(res.get(0))
          // TODO add User to chatMessage
          chatMessages.push(chatMessage)
        })
        return chatMessages
      })
  }
}
