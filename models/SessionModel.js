const User = require('./neo4j/User')
const Security = require('../library/Security')

module.exports = class SessionModel {
  constructor (neo4jDriver) {
    this.neo4jDriver = neo4jDriver
  }

  login (email, password) {
    const session = this.neo4jDriver.session()
    return session.run(
      'MATCH (a:User) WHERE a.email = {email} AND a.password = {password} ' +
      'RETURN a',
      {
        email: email,
        password: Security.passwordHash(password)
      }
    )
      .then(result => {
        session.close()
        if (!result.records.length) {
          return false
        }
        const node = result.records[0].get('a')
        return new User(node)
      })
  }
}
