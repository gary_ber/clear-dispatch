const SCWorker = require('socketcluster/scworker')
const express = require('express')
const app = express()
const path = require('path')
const healthChecker = require('sc-framework-health-check')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const Response = require(path.join(__dirname, 'library/Response'))
const compression = require('compression')
const neo4j = require('neo4j-driver').v1
const env = process.env.NODE_ENV || 'development'

process.env = require(path.join(__dirname, 'config/config.json'))[env]

if (!process.env.NEO4J_ADDRESS) process.env.NEO4J_ADDRESS = 'localhost'
if (!process.env.NEO4J_PORT) process.env.NEO4J_PORT = 7687
if (!process.env.NEO4J_USER) process.env.NEO4J_USER = 'neo4j'
if (!process.env.NEO4J_PASS) process.env.NEO4J_PASS = 'test'

class Worker extends SCWorker {
  run () {
    console.log('   >> Worker PID:', process.pid)
    app.use(logger('dev'))

    /*
      Express Middleware
    */

    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(cookieParser())
    app.use(compression())

    // Add GET /health-check express route
    healthChecker.attach(this, app)

    /*
      WebPack Dev
    */

    if (process.env.ENV === 'development') {
      app.use(favicon(path.join(__dirname, 'public/static/favicon.ico')))
      const webpack = require('webpack')
      const webpackConfig = require('./public/build/webpack.dev.conf')
      const compiler = webpack(webpackConfig)
      const devMiddleware = require('webpack-dev-middleware')(compiler, {
        publicPath: path.join(__dirname, 'public'),
        quiet: true,
        lazy: false,
        stats: {
          colors: true,
          chunks: false
        },
        watchOptions: {
          aggregateTimeout: 300,
          poll: 1000
        }
      })
      app.use(devMiddleware)
      const hotMiddleware = require('webpack-hot-middleware')(compiler, {
        log: false,
        heartbeat: 2000
      })
      app.use(hotMiddleware)
      app.use('/static', express.static(path.join(__dirname, 'public/static')))
      app.use('/', function (req, res, next) {
        const filename = path.join(__dirname, 'public/index.html')
        devMiddleware.waitUntilValid(() => {
          compiler.outputFileSystem.readFile(filename, function (err, result) {
            if (err) {
              return next(err)
            }
            res.set('content-type', 'text/html')
            res.send(result)
            res.end()
          })
        })
      })
    }

    /*
      Production Server
    */

    if (process.env.ENV === 'production') {
      app.use(favicon(path.join(__dirname, 'public/dist/static/favicon.ico')))
      app.use(express.static(path.join(__dirname, 'public/dist')))
    }

    /*
      HTTP Error
    */

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
      let err = new Error('Not Found')
      err.status = 404
      next(err)
    })

    // error handler
    app.use(function (err, req, res, next) {
      res.status(err.status || 500)
      new Response(res).addError({
        message: err.message,
        error: (process.env.ENV === 'development') ? err : {}
      }).send()
    })

    /*
      Server
    */

    const httpServer = this.httpServer
    const scServer = this.scServer

    httpServer.on('request', app)

    /*
      Neo4j
     */
    const neo4jDriver = neo4j.driver('bolt://' + process.env.NEO4J_ADDRESS + ':' + process.env.NEO4J_PORT, neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASS))

    // so the program will not close instantly
    process.stdin.resume()
    function exitHandler (options) {
      if (options.cleanup) {
        neo4jDriver.close()
      }
      if (options.exit) process.exit()
    }
    // do something when app is closing
    process.on('exit', exitHandler.bind(null, {cleanup: true}))
    // catches ctrl+c event
    process.on('SIGINT', exitHandler.bind(null, {exit: true}))
    // catches "kill pid" (for example: nodemon restart)
    process.on('SIGUSR1', exitHandler.bind(null, {exit: true}))
    process.on('SIGUSR2', exitHandler.bind(null, {exit: true}))
    // catches uncaught exceptions
    process.on('uncaughtException', exitHandler.bind(null, {exit: true}))

    /*
      Socket Controllers
    */
    require(path.join(__dirname, 'controllers'))(scServer, neo4jDriver)
  }
}

/* eslint-disable no-new */
new Worker()
