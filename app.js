const env = process.env.NODE_ENV || 'development'
const fs = require('fs')
const path = require('path')
const SocketCluster = require('socketcluster')
const http = require('http')

process.env = require(path.join(__dirname, 'config/config.json'))[env]

/*
  Certificates
 */

const certPath = (env === 'development') ? path.join(__dirname, 'cert/dev') : path.join(__dirname, 'cert/prod')
if (!process.env.ENV_CA_PATH) process.env.ENV_CA_PATH = path.join(certPath, 'bundle.crt')
if (!process.env.ENV_KEY_PATH) process.env.ENV_KEY_PATH = path.join(certPath, 'server.key')
if (!process.env.ENV_CRT_PATH) process.env.ENV_CRT_PATH = path.join(certPath, 'server.crt')

/*
  Host
 */

if (!process.env.HTTPS_PORT) process.env.HTTPS_PORT = 443
if (!process.env.HTTP_PORT) process.env.HTTP_PORT = 80
if (!process.env.REDIS_ADDRESS) process.env.REDIS_ADDRESS = 'localhost'
if (!process.env.REDIS_PORT) process.env.REDIS_PORT = 6379

/*
  SocketCluster
*/

let scConfig = {
  workers: require('os').cpus().length,
  brokers: 1,
  logLevel: 1,
  port: process.env.HTTPS_PORT,
  appName: 'clear-dispatch',
  protocol: 'https',
  workerController: path.join(__dirname, 'worker.js'),
  brokerController: path.join(__dirname, 'broker.js'),
  socketEventLimit: 500,
  rebootWorkerOnCrash: true,
  wsEngine: 'ws',
  brokerOptions: {
    host: process.env.REDIS_ADDRESS,
    port: process.env.REDIS_PORT
  },
  protocolOptions: {
    ca: fs.readFileSync(process.env.ENV_CA_PATH, 'utf8'),
    key: fs.readFileSync(process.env.ENV_KEY_PATH, 'utf8'),
    cert: fs.readFileSync(process.env.ENV_CRT_PATH, 'utf8'),
    passphrase: process.env.SESSION_SECRET
  }
}

/* eslint-disable no-new */
new SocketCluster(scConfig)

/*
  Redirect HTTP to HTTPS
 */

http.createServer((request, response) => {
  response.writeHead(301, { 'Location': 'https://' + request.headers['host'] + request.url })
  response.end()
}).listen(process.env.HTTP_PORT)
