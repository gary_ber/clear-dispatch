import GuestLayout from '@/layouts/Guest'
import SecureLayout from '@/layouts/Secure'
import LoginPage from '@/pages/Default/Login'
import RegisterPage from '@/pages/Default/Register'
import DashboardPage from '@/pages/Secure/Dashboard'
import LogoutPage from '@/pages/Secure/Logout'
import InboxPage from '@/pages/Secure/Inbox/Inbox'
import UserListPage from '@/pages/Secure/User/UserList'
import UserFormPage from '@/pages/Secure/User/UserForm'
import TruckListPage from '@/pages/Secure/Truck/TruckList'

export default [
  {
    path: '/',
    meta: {
      auth: false
    },
    component: GuestLayout,
    children: [
      {
        path: '/',
        name: 'login',
        component: LoginPage
      },
      {
        path: '/register',
        name: 'register',
        component: RegisterPage
      }
    ]
  },
  {
    path: '/secure',
    meta: {
      auth: true
    },
    component: SecureLayout,
    children: [
      {
        path: '/',
        name: 'dashboard',
        component: DashboardPage
      },
      {
        path: '/logout',
        name: 'logout',
        component: LogoutPage
      },
      {
        path: '/inbox',
        name: 'inbox',
        component: InboxPage
      },
      {
        path: '/user-list',
        name: 'user-list',
        component: UserListPage
      },
      {
        path: '/user-create',
        name: 'user-create',
        component: UserFormPage
      },
      {
        path: '/user-edit/:id',
        name: 'user-edit',
        component: UserFormPage
      },
      {
        path: '/truck-list',
        name: 'truck-list',
        component: TruckListPage
      }
    ]
  }
]
