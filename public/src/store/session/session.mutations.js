export default {
  LOGIN (state, user) {
    if (user) {
      state.authenticated = true
      state.user = user
    }
  },
  LOGOUT (state) {
    state.authenticated = false
  }
}
