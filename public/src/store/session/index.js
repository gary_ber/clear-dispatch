import actions from './session.actions'
import mutations from './session.mutations'
import getters from './session.getters'

const state = {
  authenticated: false,
  user: {
    id: 0,
    name: 'Guest',
    email: ''
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
