import Vue from 'vue'
import SessionModel from '@/models/SessionModel'

export default {
  loggedIn (context, user) {
    if (!user) return
    context.commit('LOGIN', user)
  },
  login (context, user) {
    const sessionModel = new SessionModel()
    sessionModel.create(user, (result) => {
      if (result.success) {
        context.commit('LOGIN', result.body)
        Vue.router.push({
          name: 'dashboard',
        })
      }
    })
  },
  logout ({ commit }) {
    commit('LOGOUT')
    Vue.router.push({
      name: 'login',
    })
  }
}
