import Vue from 'vue'
import Vuex from 'vuex'
import sessionModule from './session'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    sessionModule
  },
  strict: debug
})
