import Vue from 'vue'
import SCSocketCreator from 'socketcluster-client'

const connection = SCSocketCreator.connect({
  hostname: window.location.hostname,
  port: window.location.port,
  secure: true
})

Vue.$sc = connection
Object.defineProperty(Vue.prototype, '$sc', {
  value: connection
})
