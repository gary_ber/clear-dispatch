import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from '@/routes'
import store from '@/store'

Vue.use(VueRouter)

export const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (!to.matched.some(m => m.meta.auth) && store.state.sessionModule.authenticated) {
    next({
      name: 'dashboard'
    })
  } else if (to.matched.some(m => m.meta.auth) && !store.state.sessionModule.authenticated) {
    next({
      name: 'login'
    })
  } else {
    next()
  }
})

Vue.router = router

export default {
  router
}
