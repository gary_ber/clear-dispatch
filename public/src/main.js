import Vue from 'vue'
import './plugins/axios'
import './plugins/vuex'
import './plugins/vue-meta'
import './plugins/vue-chat-scroll'
import { router } from './plugins/vue-router'
import './plugins/vee-validate'
import store from './store'
import './plugins/socketcluster'
import App from './App'

Vue.config.productionTip = false

Vue.$sc.on('connect', function (status) {
  if (status.isAuthenticated) {
    const authToken = Vue.$sc.getAuthToken()
    store.dispatch('sessionModule/loggedIn', authToken.user)
  }
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
})
