import BaseModel from './Model'

export default class ChatRoomModel extends BaseModel {
  constructor () {
    super('chat_room')
  }

  toggleOpen (data, callback) {
    this.submit('toggleOpen', data, callback)
  }

  toggleListening () {
    this.submit('toggleListening', data, callback)
  }

  invite () {

  }
}
