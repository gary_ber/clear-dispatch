import BaseModel from './Model'

export default class UserModel extends BaseModel {
  constructor () {
    super('user')
  }
}
