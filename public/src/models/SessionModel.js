import BaseModel from './Model'

export default class SessionModel extends BaseModel {
  constructor () {
    super('session')
  }
}
