import Vue from 'vue'

export default class BaseModel {
  constructor (controller) {
    this.controller = controller
    this.$sc = Vue.$sc
  }

  watch (methods, uniqueChannelName) {
    let channelName = this.controller
    if (uniqueChannelName) {
      channelName = `${this.controller}-${uniqueChannelName}`
    }
    this.channel = this.$sc.subscribe(channelName, {
      waitForAuth: true
    })
    this.channel.watch((data) => {
      if (typeof methods[data.method] === 'function') {
        methods[data.method](data.data)
      }
    })
  }

  submit (method, data, callback) {
    const self = this
    return this.$sc.emit('messages', {
      controller: this.controller,
      method: method,
      data: data
    }, function(err, data) {
      if (err) {
        console.log(`Request ${self.controller} ${method} response failed...`)
        console.log(err)
        return
      }
      if (typeof callback === 'function') {
        callback(data)
      }
    })
  }

  paginate (data, callback) {
    return this.submit('paginate', {
      page: data.page,
      limit: data.limit
    }, callback)
  }

  all (data, callback) {
    return this.submit('all', data, callback)
  }

  find (id, callback) {
    return this.submit('find', {
      id: id
    }, callback)
  }

  create (data, callback) {
    return this.submit('create', data, callback)
  }

  update (data, callback) {
    return this.submit('update', data, callback)
  }

  destroy (id, callback) {
    return this.submit('destroy', {
      id: id
    }, callback)
  }
}
