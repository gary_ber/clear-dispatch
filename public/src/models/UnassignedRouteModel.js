import BaseModel from './Model'

export default class UnassignedRoute extends BaseModel {
  constructor () {
    super('unassigned_route')
  }
}
