import BaseModel from './Model'

export default class ChatMessageModel extends BaseModel {
  constructor () {
    super('chat_message')
  }
}
