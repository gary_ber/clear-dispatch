const path = require('path')
const SocketResponse = require('../library/SocketResponse')

module.exports = function (scServer, neo4jDriver) {
  let controllers = []
  // load controllers
  require('fs')
    .readdirSync(__dirname)
    .filter(function (file) {
      return (file.indexOf('.') !== 0) && (file !== 'index.js')
    })
    .forEach(function (file) {
      let name = path.basename(file, '.js').replace('Controller', '').split(/(?=[A-Z])/).join('_').toLowerCase()
      controllers[name] = require(path.join(__dirname, file))
      console.log(`Loaded Controller: ${name}\n`)
    })
  // listen
  scServer.on('connection', function (client) {
    client.on('messages', function (data, respond) {
      // Request
      let requestData
      if (data.data) {
        requestData = data.data
      }
      let requestParams
      if (data.params) {
        requestParams = data.params
      }
      const request = {
        data: requestData,
        params: requestParams
      }
      // Response
      let response = new SocketResponse(respond)
      response.setControllerName(data['controller'])
      response.setControllerMethod(data['method'])
      //
      if (!controllers[data['controller']]) {
        return respond(null, response.addError(`Controller: ${data['controller']} does not exist.`))
      }
      //
      const controller = new controllers[data['controller']](client, neo4jDriver)
      //
      if (controller.requireAuth === true && !client.getAuthToken()) {
        return respond(null, response.addError('You need to be logged in to access this controller.'))
      }
      //
      try {
        controller[data['method']](request, response)
      } catch (err) {
        console.log(err)
        if (data.controller) {
          return respond(null, response.addError(`Controller: ${data['controller']} Method: ${data['method']} is not implemented.`))
        }
      }
    })
  })
}
