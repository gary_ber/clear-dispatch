const BaseController = require('../library/Controller')

module.exports = class UnassignedRouteController extends BaseController {
  create (request, response) {
    'use strict'
    this.client.exchange.publish('unassigned_route', {
      method: 'created',
      data: {
        id: Math.round(Math.random() * 9999) + 1
      }
    })
    return response.respond()
  }

  all (request, response) {
    'use strict'
    return response.respond([
      {
        id: 1
      },
      {
        id: 2
      },
      {
        id: 3
      }
    ])
  }
}
