const BaseController = require('../library/Controller')
const ChatRoomModel = require('../models/ChatRoomModel')

module.exports = class ChatRoomController extends BaseController {
  create (request, response) {
    'use strict'
    let users = request.data.users
    users.push(this.client.getAuthToken().user)
    const chatRoomModel = new ChatRoomModel(this.neo4jDriver)
    chatRoomModel
      .create(users)
      .then(chatRoom => {
        if (!chatRoom) {
          return response
            .addError({
              message: 'ChatRoom creation failed.'
            })
            .respond()
        }
        chatRoom.attendants.forEach(attendant => {
          this.client.exchange.publish(`user-${attendant.user.uuid}`, {
            method: 'chat_room_created',
            data: chatRoom
          })
        })
        return response.respond(chatRoom)
      })
  }

  find (request, response) {
    'use strict'
    const chatRoomModel = new ChatRoomModel(this.neo4jDriver)
    chatRoomModel
      .find(request.data.chat_room_uuid)
      .then(chatRoom => {
        return response.respond(chatRoom)
      })
  }

  toggleOpen (request, response) {
    'use strict'
    const chatRoomModel = new ChatRoomModel(this.neo4jDriver)
    chatRoomModel
      .toggleOpen(
        {
          uuid: request.data.chat_room_uuid
        },
        this.client.getAuthToken().user,
        request.data.open
      )
      .then(chatRoom => {
        if (!chatRoom) {
          return response
            .addError({
              message: 'ChatRoom edit failed.'
            })
            .respond()
        }
        return response.respond(chatRoom)
      })
  }

  toggleListening (request, response) {
    'use strict'
    const chatRoomModel = new ChatRoomModel(this.neo4jDriver)
    chatRoomModel
      .toggleOpen(
        {
          uuid: request.data.chat_room_uuid
        },
        this.client.getAuthToken().user,
        request.data.listening
      )
      .then(chatRoom => {
        if (!chatRoom) {
          return response
            .addError({
              message: 'ChatRoom edit failed.'
            })
            .respond()
        }
        return response.respond(chatRoom)
      })
  }

  all (request, response) {
    'use strict'
    const chatRoomModel = new ChatRoomModel(this.neo4jDriver)
    chatRoomModel
      .all(this.client.getAuthToken().user.uuid)
      .then(chatRooms => {
        return response.respond(chatRooms)
      })
  }
}
