const BaseController = require('../library/Controller')
const validator = require('validator')
const UserModel = require('../models/UserModel')

module.exports = class UserController extends BaseController {
  create (request, response) {
    'use strict'
    if (!request.data.name) {
      return response.addError({
        message: 'Name is required.'
      })
        .respond()
    }
    if (!validator.isEmail(request.data.email)) {
      return response.addError({
        message: 'Email is not in a valid format.'
      })
        .respond()
    }
    if (!request.data.password) {
      return response.addError({
        message: 'Password is required.'
      })
        .respond()
    }
    const userModel = new UserModel(this.neo4jDriver)
    userModel
      .create(
        request.data.name,
        request.data.email,
        request.data.password
      )
      .then(user => {
        if (!user) {
          return response.addError({
            message: 'User creation failed.'
          })
            .respond()
        }
        this.client.exchange.publish('user', {
          method: 'created',
          data: user
        })
        return response.respond(user)
      })
  }

  find (request, response) {
    'use strict'
    const userModel = new UserModel(this.neo4jDriver)
    userModel
      .find(
        parseInt(request.data.id)
      )
      .then(user => {
        if (!user) {
          return response.addError({
            message: 'User not found.'
          })
            .respond()
        }
        return response.respond(user)
      })
  }

  update (request, response) {
    'use strict'
    const userModel = new UserModel(this.neo4jDriver)
    userModel
      .update(
        request.data
      )
      .then(user => {
        if (!user) {
          return response.addError({
            message: 'User not found.'
          })
            .respond()
        }
        return response.respond(user)
      })
  }

  destroy (request, response) {
    'use strict'
    const userModel = new UserModel(this.neo4jDriver)
    userModel
      .destroy(
        parseInt(request.data.id)
      )
      .then(user => {
        this.client.exchange.publish('user', {
          method: 'destroyed',
          data: user
        })
        return response.respond(user)
      })
  }

  all (request, response) {
    'use strict'
    const userModel = new UserModel(this.neo4jDriver)
    userModel
      .all()
      .then(users => {
        return response.respond(users)
      })
  }

  paginate (request, response) {
    'use strict'
    const userModel = new UserModel(this.neo4jDriver)
    userModel
      .all()
      .then(users => {
        return response.respond(users)
      })
  }
}
