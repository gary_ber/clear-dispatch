const BaseController = require('../library/Controller')
const validator = require('validator')
const SessionModel = require('../models/SessionModel')

module.exports = class SessionController extends BaseController {
  constructor (client, neo4jDriver) {
    'use strict'
    super(client, neo4jDriver)
    this.requireAuth = false
  }

  create (request, response) {
    'use strict'
    if (!validator.isEmail(request.data.email)) {
      return request.respond(response.addError({
        message: 'Email is not in a valid format.'
      }))
    }
    const sessionModel = new SessionModel(this.neo4jDriver)
    sessionModel
      .login(
        request.data.email,
        request.data.password
      )
      .then(user => {
        if (!user) {
          return response.addError({
            message: 'Email and password did not match our records.'
          })
            .respond()
        }
        this.client.setAuthToken({
          user: user
        })
        // TODO publish that a user has logged in
        // TODO publish that a user has connected
        return response.respond(user)
      })
  }

  destroy (request, response) {
    'use strict'
    this.client.deauthenticate()
    // TODO publish that a user has logged out
    // TODO publish that a user has disconnected
    return response.respond()
  }

  connected (request, response) {
    'use strict'
    // TODO publish that a user has connected
    return response.respond()
  }

  disconnected (request, response) {
    'use strict'
    // TODO publish that a user has disconnected
    return response.respond()
  }
}
