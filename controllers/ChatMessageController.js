const BaseController = require('../library/Controller')
const ChatMessageModel = require('../models/ChatMessageModel')

module.exports = class ChatRoomController extends BaseController {
  create (request, response) {
    const chatMessageModel = new ChatMessageModel(this.neo4jDriver)
    chatMessageModel
      .create(
        this.client.getAuthToken().user.uuid,
        request.data.chat_room_uuid,
        request.data.content
      )
      .then(chatMessage => {
        if (!chatMessage) {
          return response
            .addError({
              message: 'ChatMessage creation failed.'
            })
            .respond()
        }
        this.client.exchange.publish(`chat_message-chat_room-${chatMessage.chatRoom.uuid}`, {
          method: 'created',
          data: chatMessage
        })
        return response.respond(chatMessage)
      })
  }

  all (request, response) {
    'use strict'
    const chatMessageModel = new ChatMessageModel(this.neo4jDriver)
    chatMessageModel
      .all(
        request.data.chat_room_uuid
      )
      .then(chatMessages => {
        return response.respond(chatMessages)
      })
  }
}
